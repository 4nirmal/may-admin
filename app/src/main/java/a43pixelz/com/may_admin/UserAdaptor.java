package a43pixelz.com.may_admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;



/**
 * Created by Vinothkannan on 17-03-2018.
 */
public class UserAdaptor extends ArrayAdapter<DataModel_Chat_User> implements View.OnClickListener{

    private ArrayList<DataModel_Chat_User> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView email;
        TextView message;
        TextView count;
        TextView status;
        TextView count2;
        LinearLayout chat_back;
    }

    public UserAdaptor(ArrayList<DataModel_Chat_User> data, Context context) {
        super(context, R.layout.chat_list_item, data);
        this.dataSet=data;
        this.mContext=context;
        this.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel_Chat_User DataModel_Chat_User=(DataModel_Chat_User)object;
    }

    private int lastPosition = 0;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DataModel_Chat_User DataModel_Chat_User = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.chat_list_item, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.chat_username);
            viewHolder.email = (TextView) convertView.findViewById(R.id.email_id);
            viewHolder.message = (TextView) convertView.findViewById(R.id.chat_message);
            viewHolder.status = (TextView) convertView.findViewById(R.id.chat_status);
            viewHolder.count = (TextView) convertView.findViewById(R.id.count);
            viewHolder.count2 = (TextView) convertView.findViewById(R.id.textView10);
            viewHolder.chat_back = (LinearLayout) convertView.findViewById(R.id.chat_back);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.email.setText(String.valueOf(DataModel_Chat_User.getemail()));
        viewHolder.name.setText(String.valueOf(DataModel_Chat_User.getname()));
        viewHolder.message.setText(String.valueOf(DataModel_Chat_User.getmessage()));
        viewHolder.count.setText(String.valueOf(position));
        if(!DataModel_Chat_User.getstatus().matches("")){
            viewHolder.count2.setText("Last Update:"+DataModel_Chat_User.getstatus());
            if(DataModel_Chat_User.getstatus().matches("REPLIED")){
                viewHolder.chat_back.setBackgroundColor(android.graphics.Color.rgb(155, 255, 155));
            }else if(DataModel_Chat_User.getstatus().matches("Success")||DataModel_Chat_User.getstatus().matches("Failed")){

            }
            else{
                viewHolder.chat_back.setBackgroundColor(android.graphics.Color.rgb(255, 155, 155));

            }
        }
        else{

            viewHolder.count2.setText("");
        }
try {
    viewHolder.status.setText(DateFormat.getDateTimeInstance().format(Long.parseLong(DataModel_Chat_User.gettime())));
}
catch (Exception e){
    viewHolder.status.setText("");
        }

        // Return the completed view to render on screen
        return convertView;
    }
}