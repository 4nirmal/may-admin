package a43pixelz.com.may_admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import a43pixelz.com.may_admin.handlers.FireCallBack;
import a43pixelz.com.may_admin.handlers.Profile_handler;
import a43pixelz.com.may_admin.modal.Profile;

public class UserActivity extends AppCompatActivity {
    EditText pemail;
    EditText pname;
    EditText edittext;
    EditText timelbl;
    TextView gender;
    TextView location;
    RadioButton accurate;
    RadioButton notaccurate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user);

        //findview
        pemail=findViewById(R.id.pemail);
        pname=findViewById(R.id.pname);
        edittext=  findViewById(R.id.birthdate);
        timelbl=findViewById(R.id.birthtime);
        accurate=findViewById(R.id.accurate);
        notaccurate=findViewById(R.id.notaccurate);
        gender=findViewById(R.id.gender);
        location=findViewById(R.id.locate);

        SPref sPref=new SPref(getApplicationContext());

        String userid=sPref.getValue("userid");
        new Profile_handler().get(userid, new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                Profile p = (Profile) value;
                pname.setText(p.getV("name"));
                pemail.setText(p.getV("email"));
                edittext.setText(p.getV("dob"));
                timelbl.setText(p.getV("time"));
                gender.setText(p.getV("gender"));
                location.setText(p.getV("location"));
                try{ if(p.getV("accurate").matches("true")){
                    accurate.setChecked(true);}else{
                    notaccurate.setChecked(true);
                }
                }catch (Exception ex){

                }
            }});
        String userid2;
        userid2=sPref.getValue("userid");
        userid=userid2;
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(sPref.getValue("username2"));
            getSupportActionBar().setSubtitle(userid);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
