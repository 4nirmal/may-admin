package a43pixelz.com.may_admin;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import a43pixelz.com.may_admin.modal.ChatMessage;

import static a43pixelz.com.may_admin.handlers.Fire_handler.db;


public class Push_Message extends Service {
    public Push_Message() {
    }
    SPref s;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }
    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }

    /**
     * Called by the system to notify a Service that it is no longer used and is being removed.  The
     * service should clean up any resources it holds (threads, registered
     * receivers, etc) at this point.  Upon return, there will be no more calls
     * in to this Service object and it is effectively dead.  Do not call this method directly.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

String CHANNEL_ID="123";
    private void createNotification() {
            createNotificationChannel();
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.star2)
                    .setContentTitle("Admin you have new Message")
                    .setContentText("Tap to view the message")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(123, mBuilder.build());

    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "AdminName";
            String description = "AdminDescription";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }int count;

    //
    private  void loadFireData()
    {

        DatabaseReference mFirebaseDatabase = db.getReference("chatcount");
        mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!s.getValue("messagecount").matches(String.valueOf(dataSnapshot.child("count").getValue()))){
                    createNotification();
                    s.setValue("messagecount",String.valueOf(dataSnapshot.child("count").getValue()));
                }
                fireWatcher("");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void fireWatcher(String Ses)
    {
        //ToDo: change this
        db.getReference("chatcount").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!s.getValue("messagecount").matches(String.valueOf(dataSnapshot.child("count").getValue()))){
                    createNotification();
                    s.setValue("messagecount",String.valueOf(dataSnapshot.child("count").getValue()));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    @Override
    public void onTaskRemoved(Intent rootIntent) {
        try{
            Intent restartService=new Intent(getApplicationContext(),Push_Message.class);
            restartService.setPackage(getPackageName());
            startService(restartService);}
        catch (Exception e){

        }
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onCreate() {
        s=new SPref(getApplicationContext());
        Toast.makeText(this, "Sastra Admin service started "+s.getValue("messagecount"), Toast.LENGTH_SHORT).show();
        if(s.getValue("messagecount").matches("null")){
            s.setValue("messagecount","0");
        }
        loadFireData();

        super.onCreate();
    }
}
