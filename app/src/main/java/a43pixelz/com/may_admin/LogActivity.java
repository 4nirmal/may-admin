package a43pixelz.com.may_admin;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import a43pixelz.com.may_admin.handlers.FireCallBack;

public class LogActivity extends AppCompatActivity {
    ListView listView;
    SPref sPref;
    String userid;
    private static customlistview adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        final ArrayList<listitemmodal> dataModels;
        dataModels= new ArrayList<>();
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.show();
        listView=findViewById(R.id.view_loglist);
        sPref=new SPref(this);
        String userid2;
        userid2=sPref.getValue("userid");
        userid=userid2;
        new Logs_Handler().get( userid, new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                ArrayList<Log> Res = (ArrayList<Log>) value;
                for(Object log : Res){
                    a43pixelz.com.may_admin.Log Log2 = (a43pixelz.com.may_admin.Log) log;
                    dataModels.add(new listitemmodal(String.valueOf(Log2.getV("status")),String.valueOf(Log2.getV("datetime")),String.valueOf(Log2.getV("question")),String.valueOf(Log2.getV("amount"))));

                }
                adapter= new customlistview(dataModels,getApplicationContext());

                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        });
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(sPref.getValue("username2"));
            getSupportActionBar().setSubtitle(userid);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
