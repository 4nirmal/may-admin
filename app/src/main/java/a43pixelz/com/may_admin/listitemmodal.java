package a43pixelz.com.may_admin;

public class listitemmodal {
    String status;
    String amount;
    String question;
    String datetime;
    public listitemmodal(String status,String datetime,String question,String amount){
        this.status=status;
        this.datetime=datetime;
        this.question=question;
        this.amount=amount;
    }
    public String getstatus() {
        return status;
    }
    public String getamount() {
        return amount;
    }
    public String getdatetime() {
        return datetime;
    }
    public String getquestion() {
        return question;
    }
}
