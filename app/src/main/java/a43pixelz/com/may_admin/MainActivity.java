package a43pixelz.com.may_admin;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;

import a43pixelz.com.may_admin.handlers.Fire_handler;
import a43pixelz.com.may_admin.modal.ChatMessage;
import a43pixelz.com.may_admin.modal.Status;
import a43pixelz.com.may_admin.modal.UserType;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Fire_handler {
    private ArrayList<ChatMessage> chatMessages;
    private ChatListAdapter listAdapter;
    private ListView chatListView;
    private EditText chatEditText1;
    private ImageView enterChatView1;
    DatabaseReference mFirebaseDatabase;
    int fireIndex = 0;

    String Self,Other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });



        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        chatMessages = new ArrayList<>();
        chatListView = (ListView) findViewById(R.id.chat_list_view);
        chatEditText1 = (EditText) findViewById(R.id.chat_edit_text1);
        enterChatView1 = (ImageView) findViewById(R.id.broadcast_send);

        listAdapter = new ChatListAdapter(chatMessages, this);
        chatListView.setAdapter(listAdapter);
        chatEditText1.setOnKeyListener(keyListener);
        enterChatView1.setOnClickListener(clickListener);
        chatEditText1.addTextChangedListener(watcher1);
        loadFireData();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
//        fireWatcher("");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void fireWatcher(String Ses)
    {
        //ToDo: change this
        db.getReference("chats").child("nirmal").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i=0;
                for (DataSnapshot msgSnapshot : dataSnapshot.getChildren()) {

                    ChatMessage msg = msgSnapshot.getValue(ChatMessage.class);
                    try{
                        chatMessages.get(i).setMessageText(msg.getMessageText());
                        if (!msg.getUser().trim().equals("nirmal")) {
                            chatMessages.get(i).setUserType(UserType.SELF);
                        } else {
                            chatMessages.get(i).setUserType(UserType.OTHER);
                        }
                        chatMessages.get(i).setUser(msg.getUser());
                        chatMessages.get(i).setMessageStatus(Status.DELIVERED);
                        i++;
                    }catch (Exception e) {

                        if (!msg.getUser().trim().equals("nirmal")) {
                            msg.setUserType(UserType.SELF);
                        } else {
                            msg.setUserType(UserType.OTHER);
                        }
                        chatMessages.add(msg);
                    }
                };
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                        chatListView.smoothScrollToPosition(listAdapter.getCount()-1);
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }

    private  void loadFireData()
    {
        if(chatMessages != null)
            chatMessages.clear();

        mFirebaseDatabase = db.getReference("chats").child("nirmal");
        mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot msgSnapshot: dataSnapshot.getChildren()) {
                    ChatMessage msg = msgSnapshot.getValue(ChatMessage.class);
                    if(!msg.getUser().trim().equals(Self)){
                        if(Other ==null){
                            Other = msg.getUser();
                        }
                        msg.setUserType(UserType.SELF);
                    }
                    else {
                        msg.setUserType(UserType.OTHER);
                    }
                    msg.setMessageStatus(Status.DELIVERED);
                    chatMessages.add(msg);
                    fireIndex++;
                };
                fireWatcher("");
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                        chatListView.smoothScrollToPosition(listAdapter.getCount()-1);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private ImageView.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==enterChatView1)
            {
                sendMessage(chatEditText1.getText().toString(), UserType.OTHER);
            }
            chatEditText1.setText("");
        }
    };

    private EditText.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                EditText editText = (EditText) v;
                if(v==chatEditText1)
                {
                    sendMessage(editText.getText().toString(), UserType.OTHER);
                }
                chatEditText1.setText("");
                return true;
            }
            return false;
        }
    };

    private void sendMessage(final String messageText, final UserType userType)
    {
        if(messageText.trim().length()==0)
            return;

        //ToDo: Change this
        fireBaseHandler(messageText, userType, "nirmal");
    }

    private void fireBaseHandler(final String messageText, final UserType userType, String ses)
    {
        ChatMessage message = new ChatMessage();
        message.setMessageStatus(Status.SENT);
        message.setMessageText(messageText);
        message.setUserType(userType);
        message.setMessageTime(new Date().getTime());
        //ToDo: change this
        message.setUser("nirmal");
        chatMessages.add(message);
        //ToDo: change this
        db.getReference("chats").child("nirmal").setValue(listAdapter.getMessages());


    }

    private final TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            sendMsg(chatEditText1.getText().toString());
            if (chatEditText1.getText().toString().equals("")) {

            } else {

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(editable.length()==0){

            }else{

            }
        }
    };
}
