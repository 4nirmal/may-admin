package a43pixelz.com.may_admin;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.Transaction;

public class HomeActivity extends AppCompatActivity implements ProfileFragment.OnFragmentInteractionListener
, LogFragment.OnFragmentInteractionListener, ChatListFragment.OnFragmentInteractionListener{

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            t= getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_chat:
                    t.replace(R.id.maincontainer, new ChatListFragment());
                    t.commit();
                    return true;
                case R.id.navigation_log:
                    t.replace(R.id.maincontainer, new LogFragment());
                    t.commit();
                    return true;
                case R.id.navigation_users:
                    t.replace(R.id.maincontainer, new UserListFragment());
                    t.commit();
                    return true;
                case R.id.navigation_profile:
                    t.replace(R.id.maincontainer, new ProfileFragment());
                    t.commit();
                    return true;
                case R.id.navigation_broadcast:
                    t.replace(R.id.maincontainer, new BroadcastFragment());
                    t.commit();
                    return true;
            }
            return false;
        }
    };

    FragmentTransaction t;
    private void autostartcheck(){
        String xiaomi = "Xiaomi";
        final String CALC_PACKAGE_NAME = "com.miui.securitycenter";
        if(sPref.getValue("autostart").matches("")||sPref.getValue("autostart").matches("null")){
            final String CALC_PACKAGE_ACITIVITY = "com.miui.permcenter.autostart.AutoStartManagementActivity";
            if (Build.MANUFACTURER.equalsIgnoreCase(xiaomi)) {
                Toast.makeText(this, "Please choose astro sastra", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(CALC_PACKAGE_NAME, CALC_PACKAGE_ACITIVITY));
                startActivity(intent);
                sPref.setValue("autostart","checked");
            }
        }

    }SPref sPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sPref=new SPref(this);
        if(getSupportActionBar()!=null) {
            if(sPref.getValue("admin_login").matches("yes")&&!sPref.getValue("admin_email").matches("")){
                getSupportActionBar().setTitle(sPref.getValue("admin_name"));
            }
            t= getSupportFragmentManager().beginTransaction();
                    t.replace(R.id.maincontainer, new ChatListFragment());
                    t.commit();
        }
        autostartcheck();
        Intent intent1=new Intent(getApplicationContext(),Push_Message.class);
        intent1.setPackage(getPackageName());
        startService(intent1);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
