package a43pixelz.com.may_admin;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Nirmal on 6/17/2018.
 */

public class SPref {
    Context ctx;

    public SPref(Context ctx){
        this.ctx = ctx;
    }

    public void setValue(String Key, String Value){
        SharedPreferences.Editor editor = ctx.getSharedPreferences("may_admin", MODE_PRIVATE).edit();
        editor.putString(Key, Value);
        editor.apply();
    }

    public String getValue(String Key){
        SharedPreferences prefs = ctx.getSharedPreferences("may_admin", MODE_PRIVATE);
        return prefs.getString(Key, "null");
    }
}
