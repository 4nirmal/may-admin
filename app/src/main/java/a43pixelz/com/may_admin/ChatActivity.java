package a43pixelz.com.may_admin;

import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;

import a43pixelz.com.may_admin.handlers.FireCallBack;
import a43pixelz.com.may_admin.handlers.Profile_handler;
import a43pixelz.com.may_admin.modal.ChatMessage;
import a43pixelz.com.may_admin.modal.Profile;
import a43pixelz.com.may_admin.modal.Status;
import a43pixelz.com.may_admin.modal.UserType;

import static a43pixelz.com.may_admin.handlers.Fire_handler.db;

public class ChatActivity extends AppCompatActivity {
    private ArrayList<ChatMessage> chatMessages;
    private ChatListAdapter listAdapter;
    private ListView chatListView;
    private EditText chatEditText1;
    private ImageView enterChatView1;
    DatabaseReference mFirebaseDatabase;
    int fireIndex = 0;
    SPref sPref;
    String admin_email;
    String admin_name;
    String userid;
    String Self,Other;
    ImageView show;
    AlertDialog alert;
    Button givefree;
    Button setcategory;
    Spinner RasiSpinner;
    Spinner LagnamSpinner;
    Spinner DasaSpinner;
    Spinner PugthiSpinner;
    EditText yeartext;
    FloatingActionButton floatingActionButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        chatMessages = new ArrayList<>();
        chatListView = (ListView) findViewById(R.id.chat_list_view);
        chatEditText1 = (EditText) findViewById(R.id.chat_edit_text1);
        enterChatView1 = (ImageView) findViewById(R.id.broadcast_send);

        listAdapter = new ChatListAdapter(chatMessages, this);
        chatListView.setAdapter(listAdapter);
        chatEditText1.setOnKeyListener(keyListener);
        enterChatView1.setOnClickListener(clickListener);
        chatEditText1.addTextChangedListener(watcher1);
        sPref=new SPref(this);
        final String userid2;
        userid2=sPref.getValue("userid");
        userid=getUserId(userid2);
        admin_email=getUserId(sPref.getValue("admin_email"));
        admin_name=sPref.getValue("admin_name");
        show=findViewById(R.id.showuser);
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View promptsView=getView();
        final BottomSheetDialog dialog = new BottomSheetDialog(ChatActivity.this);
        dialog.setContentView(promptsView);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.show();
            }
        });
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView2 = li.inflate(R.layout.segregate_popup, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView2);

        alert = alertDialogBuilder.create();
        alert.setCancelable(true);

        floatingActionButton=findViewById(R.id.extra_buttton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.show();
                create_dialog2();
            }
        });
        loadFireData();
        new Profile_handler().get(userid, new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                Profile p = (Profile) value;
                //s2.setValue("username",p.getV("name"));
                if(getSupportActionBar()!=null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setTitle(p.getV("name"));
                    getSupportActionBar().setSubtitle(userid);
                }
            }});

    }
    TextView list_it;
    public void create_dialog2(){
        givefree=(Button) alert.findViewById(R.id.give_free);

        String[] rasi1=new String[]{"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருச்சிகம்","தனுசு","மகரம்","கும்பம்","மீனம்"};
        ArrayAdapter<String> rasi=new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,rasi1);
        rasi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        RasiSpinner=(Spinner)alert.findViewById(R.id.rasispinner);
        RasiSpinner.setAdapter(rasi);

        String[] lagnam1=new String[]{"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருச்சிகம்","தனுசு","மகரம்","கும்பம்","மீனம்"};
        ArrayAdapter<String> lagnam=new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,lagnam1);
        lagnam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        LagnamSpinner=(Spinner)alert.findViewById(R.id.lagnamspinner);
        LagnamSpinner.setAdapter(lagnam);

        String[] Dasa1=new String[]{"சூரியன்","சந்திரன்","செவ்வாய்","புதன்","குரு","சுக்கிரன்","சனி","ராகு","கேது"};
        ArrayAdapter<String> Dasa=new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,Dasa1);
        Dasa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        DasaSpinner=(Spinner)alert.findViewById(R.id.dasaspinner);
        DasaSpinner.setAdapter(Dasa);

        yeartext=(EditText) alert.findViewById(R.id.yearText);
        list_it=(TextView)alert.findViewById(R.id.List_It);
        new Profile_handler().get(userid, new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                Profile p = (Profile) value;

                list_it.setText(p.getV("rasi")+"\n\n\n"+p.getV("lagnam")+"\n\n\n"+p.getV("dasa")+"\n\n\n\n\n"+p.getV("year"));
            }});
        setcategory=(Button)alert.findViewById(R.id.subCategory);
        setcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Profile_handler().get(sPref.getValue("userid"), new FireCallBack() {
                    @Override
                    public void onCallback(Object value) {
                        Profile p = (Profile) value;
                        p.setV("rasi",RasiSpinner.getSelectedItem().toString());
                        p.setV("lagnam",LagnamSpinner.getSelectedItem().toString());
                        p.setV("dasa",DasaSpinner.getSelectedItem().toString());
                        p.setV("year",yeartext.getText().toString());

                        new Profile_handler().write(p);
                        alert.dismiss();


                    }
                });
            }
        });

        givefree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChatMessage message = new ChatMessage();
                message.setMessageStatus(Status.AUTO_REPLY);
                message.setMessageText("Hi, <br>You have earned one extra free question.<br><br>Thank you.");
                message.setUserType(UserType.OTHER);
                message.setMessageTime(new Date().getTime());
                //ToDo: change this
                message.setUser(admin_name);
                chatMessages.add(message);
                //ToDo: change this
                db.getReference("chats").child(userid).setValue(listAdapter.getMessages());
                new Profile_handler().get(sPref.getValue("userid"), new FireCallBack() {
                    @Override
                    public void onCallback(Object value) {
                        Profile p = (Profile) value;
                        p.setV("free_question","1");
                        new Profile_handler().write(p);
                        alert.dismiss();


                    }
                });
            }
        });
    }
    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private static class ViewHolder {
        TextView name;
        TextView DOB;
        TextView TOB;
        TextView accurate;
        TextView gender;
        TextView location;
    }

    public View getView() {

        final ViewHolder viewHolder;
        View convertView;
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
            convertView = inflater.inflate(R.layout.layout, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.pop_username);
            viewHolder.DOB = (TextView) convertView.findViewById(R.id.pop_DOB);
            viewHolder.TOB = (TextView) convertView.findViewById(R.id.pop_TOB);
            viewHolder.accurate = (TextView) convertView.findViewById(R.id.pop_accurate);
            viewHolder.gender = (TextView) convertView.findViewById(R.id.pop_gender);
            viewHolder.location = (TextView) convertView.findViewById(R.id.pop_location);

            convertView.setTag(viewHolder);

        new Profile_handler().get(sPref.getValue("userid"), new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                Profile p = (Profile) value;
                viewHolder.DOB.setText(p.getV("dob"));
                viewHolder.name.setText(p.getV("name"));
                viewHolder.TOB.setText(p.getV("time"));
                viewHolder.accurate.setText(p.getV("accurate"));
                viewHolder.gender.setText(p.getV("gender"));
                viewHolder.location.setText(p.getV("location"));
            }});

        // Return the completed view to render on screen
        return convertView;
    }

    private void fireWatcher(String Ses)
    {
        //ToDo: change this
        db.getReference("chats").child(userid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i=0;
                for (DataSnapshot msgSnapshot : dataSnapshot.getChildren()) {
                    ChatMessage msg = msgSnapshot.getValue(ChatMessage.class);
                    try{
                         chatMessages.get(i).setMessageText(msg.getMessageText());
                        if (!msg.getUser().trim().equals(admin_name)) {
                            chatMessages.get(i).setUserType(UserType.SELF);
                        } else {
                            chatMessages.get(i).setUserType(UserType.OTHER);
                        }
                        chatMessages.get(i).setUser(msg.getUser());
                        chatMessages.get(i).setMessageStatus(Status.DELIVERED);
                        i++;
                    }
                    catch (Exception e) {

                        if (!msg.getUser().trim().equals(admin_name)) {
                            msg.setUserType(UserType.SELF);
                        } else {
                            msg.setUserType(UserType.OTHER);
                        }
                        chatMessages.add(msg);
                    }
                };
                ChatActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                        chatListView.smoothScrollToPosition(listAdapter.getCount()-1);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }

    private  void loadFireData()
    {
        if(chatMessages != null)
            chatMessages.clear();

        mFirebaseDatabase = db.getReference("chats").child(userid);
        mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot msgSnapshot: dataSnapshot.getChildren()) {
                    ChatMessage msg = msgSnapshot.getValue(ChatMessage.class);
                    if(!msg.getUser().trim().equals(Self)){
                        if(Other ==null){
                            Other = msg.getUser();
                        }
                        msg.setUserType(UserType.SELF);
                    }
                    else {
                        msg.setUserType(UserType.OTHER);
                    }
                    msg.setMessageStatus(Status.DELIVERED);
                    chatMessages.add(msg);
                    fireIndex++;
                }
                fireWatcher("");
                ChatActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                        chatListView.smoothScrollToPosition(listAdapter.getCount()-1);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private ImageView.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==enterChatView1)
            {
                sendMessage(chatEditText1.getText().toString(), UserType.OTHER);
            }
            chatEditText1.setText("");

        }
    };

    private EditText.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                EditText editText = (EditText) v;
                if(v==chatEditText1)
                {
                    sendMessage(editText.getText().toString(), UserType.OTHER);
                }
                chatEditText1.setText("");
                return true;
            }
            return false;
        }
    };

    private void sendMessage(final String messageText, final UserType userType)
    {
        if(messageText.trim().length()==0)
            return;

        //ToDo: Change this
        fireBaseHandler(messageText, userType, "");
    }

    private void fireBaseHandler(final String messageText, final UserType userType, String ses)
    {
        ChatMessage message = new ChatMessage();
        message.setMessageStatus(Status.REPLIED);
        message.setMessageText(messageText);
        message.setUserType(userType);
        message.setMessageTime(new Date().getTime());
        //ToDo: change this
        message.setUser(admin_name);
        chatMessages.add(message);
        //ToDo: change this
        db.getReference("chats").child(userid).setValue(listAdapter.getMessages());


    }

    private final TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            sendMsg(chatEditText1.getText().toString());
            if (chatEditText1.getText().toString().equals("")) {

            } else {

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(editable.length()==0){

            }else{

            }
        }
    };
}
