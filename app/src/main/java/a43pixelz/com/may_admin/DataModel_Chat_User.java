package a43pixelz.com.may_admin;

import android.support.annotation.NonNull;

public class DataModel_Chat_User {
    String user;
    String email;
    String status;
    String message;
    String time;
    public DataModel_Chat_User(String username, String email, String message, String time,String status) {
        this.user=username;
        this.email=email;
        this.time=time;
        this.status=status;
        this.message=message;
    }

    public String getname() {
        return user;
    }
    public String getemail() {
        return email;
    }
    public String getstatus() {
        return status;
    }
    public String getmessage() {
        return message;
    }
    public String gettime() {
        return time;
    }


}
