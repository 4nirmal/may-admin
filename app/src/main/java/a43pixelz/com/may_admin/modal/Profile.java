package a43pixelz.com.may_admin.modal;

import java.util.HashMap;
import java.util.Map;

public class Profile {
    public Map<String, String> kv ;

    public Profile(){
        kv = new HashMap<>();
    }
    public Profile(String name, String  email, String  dob,String gender, String time, String acc, String loc,String free,String paid,String rasi,String lagnam,String thisai,String year){
        kv = new HashMap<>();
        kv.put("name", name);
        kv.put("email", email);
        kv.put("dob", dob);
        kv.put("gender", gender);
        kv.put("time", time);
        kv.put("accurate", acc);
        kv.put("location", loc);
        kv.put("free_question", free);
        kv.put("paid_question", paid);
        kv.put("rasi", rasi);
        kv.put("lagnam", lagnam);
        kv.put("dasa", thisai);
        kv.put("year", year);

    }

    public String getV(String K){
        return kv.get(K);
    }
    public Object getKV(){
        return kv;
    }
    public void setV(String K, String V) { kv.put(K, V);}
}
