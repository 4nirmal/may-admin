package a43pixelz.com.may_admin.modal;

/**
 * Created by Nirmal on 7/23/2018.
 */

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nirmal on 8/5/18.
 */

public class Admin {
    Map<String, String> kv ;

    public Admin(){
        kv = new HashMap<>();
    }
    public Admin(String name, String  email, String  password){
        kv = new HashMap<>();
        kv.put("name", name);
        kv.put("email", email);
        kv.put("password", password);
    }

    public String getV(String K){
        return kv.get(K);
    }
    public Object getKV(){
        return kv;
    }
}
