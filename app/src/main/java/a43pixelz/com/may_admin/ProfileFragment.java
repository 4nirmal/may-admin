package a43pixelz.com.may_admin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import a43pixelz.com.may_admin.handlers.Admin_Handler;
import a43pixelz.com.may_admin.handlers.FireCallBack;
import a43pixelz.com.may_admin.handlers.Profile_handler;
import a43pixelz.com.may_admin.modal.Admin;
import a43pixelz.com.may_admin.modal.Profile;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    EditText username;
    EditText email;
    EditText password;
    TextView edit_btn;
    TextView cancel_btn;
    Button choosephoto;
    ImageView profilepicture;
    Button logout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }



    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //findView
        username=(EditText)view.findViewById(R.id.profile_username);
        email=(EditText)view.findViewById(R.id.profile_email);
        password=(EditText)view.findViewById(R.id.profile_password);
        edit_btn=(TextView)view.findViewById(R.id.profile_edit_btn);
        cancel_btn=(TextView)view.findViewById(R.id.profile_cancel);
        choosephoto=(Button) view.findViewById(R.id.profile_choosephoto);
        profilepicture=(ImageView)view.findViewById(R.id.profilephoto);
        logout=(Button) view.findViewById(R.id.logout);

        //Initial action
        username.setEnabled(false);
        email.setEnabled(false);
        password.setEnabled(false);
        choosephoto.setEnabled(false);

        //update Text
        final SPref sPref=new SPref(getActivity().getApplicationContext());
        String userid=sPref.getValue("admin_email");
        new Admin_Handler().get(userid, new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                Admin p = (Admin) value;
                sPref.setValue("email",p.getV("email"));
                sPref.setValue("username",p.getV("name"));
                username.setText(p.getV("name"));
                email.setText(p.getV("email"));
                password.setText(p.getV("password"));
            }});
        //Listener
        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edittext;
                edittext=edit_btn.getText().toString();
                if(edittext.matches("Edit")){
                    edit_btn.setText("Save");
                    username.setEnabled(true);
                    email.setEnabled(true);
                    password.setEnabled(true);
                    choosephoto.setEnabled(true);
                    cancel_btn.setVisibility(View.VISIBLE);

                }else{
                    edit_btn.setText("Edit");
                    username.setEnabled(false);
                    email.setEnabled(false);
                    password.setEnabled(false);
                    choosephoto.setEnabled(false);
                    cancel_btn.setVisibility(View.INVISIBLE);
                }
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edit_btn.setText("Edit");
                username.setEnabled(false);
                email.setEnabled(false);
                password.setEnabled(false);
                choosephoto.setEnabled(false);
                cancel_btn.setVisibility(View.INVISIBLE);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sPref.setValue("admin_login","no");
                sPref.setValue("admin_email","");
                sPref.setValue("admin_name","");
                Intent intent=new Intent(getActivity().getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        return rootView;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
