package a43pixelz.com.may_admin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.UserHandle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import a43pixelz.com.may_admin.handlers.FireCallBack;
import a43pixelz.com.may_admin.handlers.Profile_handler;
import a43pixelz.com.may_admin.handlers.Users_Handler;
import a43pixelz.com.may_admin.handlers.chat_list_handler;
import a43pixelz.com.may_admin.modal.Profile;
import a43pixelz.com.may_admin.modal.User;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChatListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChatListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ChatListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatListFragment newInstance(String param1, String param2) {
        ChatListFragment fragment = new ChatListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chat_list, container, false);


    return view;
    }
ProgressDialog progressDialog;
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */

    int rep=0;
    int tot=0;
    String replayed="Replied: ";
    String total="Total: ";
    String Pending="Pending: ";
    ListView listView;
    UserAdaptor adapter;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final ArrayList<DataModel_Chat_User> DataModel_Chat_Users;
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final View V = view;
        DataModel_Chat_Users= new ArrayList<>();
        new chat_list_handler().getUsers(new FireCallBack() {
            @Override
            public void onCallback(Object value) {
               final  List<User> Users = (List<User>) value;
                for (User U : Users){
                    DataModel_Chat_Users.add(new DataModel_Chat_User(U.getV("name"),U.getV("email"),U.getV("password"),U.getV("mac"),U.getV("status")));
                    if(U.getV("status").matches("REPLIED")){
                        rep = rep +1;
                    }
                    tot=tot+1;
                }
                //hear we have to sort
                  Collections.sort(DataModel_Chat_Users,new Comparator<DataModel_Chat_User>() {
                      @Override
                      public int compare(DataModel_Chat_User s1, DataModel_Chat_User s2) {
                          return s2.time.compareTo(s1.time);
                      }
                  });
// Updated upstream
                total += tot;
                TextView totl=V.findViewById(R.id.count_total);
                totl.setText(String.valueOf(total));
                replayed += rep;
                TextView repl=V.findViewById(R.id.count_replayed);
                repl.setText(replayed);
                Pending += String.valueOf(tot - rep);
                TextView pend=V.findViewById(R.id.count_pending);
                pend.setText(Pending);

                adapter= new UserAdaptor(DataModel_Chat_Users,getActivity().getApplicationContext());
                listView=(ListView)V.findViewById(R.id.chat_user_list_item);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
//
progressDialog.dismiss();
            }
        });
        adapter= new UserAdaptor(DataModel_Chat_Users,getActivity());
        listView=(ListView)view.findViewById(R.id.chat_user_list_item);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setAdapter(adapter);
                SPref sPref=new SPref(getActivity());
                DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                sPref.setValue("userid",dataModel.getemail());
                sPref.setValue("username2",dataModel.getname());
                Intent intent=new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
            }
        });
        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
// Stashed changes
                adapter.notifyDataSetChanged();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SPref sPref=new SPref(getActivity().getApplicationContext());
                        DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                        sPref.setValue("userid",dataModel.getemail());
                        sPref.setValue("username2",dataModel.getname());
                        Intent intent=new Intent(getActivity().getApplicationContext(),ChatActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
