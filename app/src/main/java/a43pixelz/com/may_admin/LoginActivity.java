package a43pixelz.com.may_admin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.TabItem;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import a43pixelz.com.may_admin.handlers.Admin_Handler;
import a43pixelz.com.may_admin.handlers.Device_Handler;
import a43pixelz.com.may_admin.handlers.FireCallBack;
import a43pixelz.com.may_admin.handlers.Login_Handler;
import a43pixelz.com.may_admin.handlers.Profile_handler;
import a43pixelz.com.may_admin.handlers.Register_Handler;
import a43pixelz.com.may_admin.modal.Admin;
import a43pixelz.com.may_admin.modal.LoginResult;
import a43pixelz.com.may_admin.modal.Profile;
import a43pixelz.com.may_admin.modal.User;
import a43pixelz.com.may_admin.util.AppUtil;

public class LoginActivity extends AppCompatActivity {

    EditText login_email;
    EditText login_password;
    EditText register_username;
    EditText register_email;
    EditText register_password;
    Button login_btn;
    Button register_btn;
    ScrollView scrollView;
    a43pixelz.com.may_admin.modal.LoginResult Res;
    Button lo;
    Button re;
    Admin UserData;
    //
    LinearLayout ll;
    LinearLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle("Welcome");

        }
        final SPref sPref=new SPref(this);

        if(sPref.getValue("admin_login").matches("yes")&&!sPref.getValue("admin_email").matches("")){
            String userid=sPref.getValue("admin_email");
            new Admin_Handler().get(userid, new FireCallBack() {
                @Override
                public void onCallback(Object value) {
                    Admin p = (Admin) value;
                    sPref.setValue("admin_email",p.getV("email"));
                    sPref.setValue("admin_name",p.getV("name"));
                    sPref.getValue("admin_email");
                    sPref.getValue("admin_name");
                }});
            finish();
            Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
            startActivity(intent);
        }
        lo=findViewById(R.id.login_clink);
        re=findViewById(R.id.reg_clink);
        ll=findViewById(R.id.login_layout);
        rl=findViewById(R.id.register_layout);
        re.setBackgroundColor(android.graphics.Color.TRANSPARENT);
        lo.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.back));

        lo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl.setVisibility(View.GONE);
                ll.setVisibility(View.VISIBLE);
                re.setBackgroundColor(android.graphics.Color.TRANSPARENT);
                lo.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.back));
            }
        });
        re.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl.setVisibility(View.VISIBLE);
                ll.setVisibility(View.GONE);
                lo.setBackgroundColor(android.graphics.Color.TRANSPARENT);
                re.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.back));
            }
        });

         login_email=findViewById(R.id.login_email);
         login_password=findViewById(R.id.login_password);
         register_username=findViewById(R.id.register_username);
         register_email=findViewById(R.id.register_email);
         register_password=findViewById(R.id.register_password);
         login_btn=findViewById(R.id.login_btn);
         register_btn=findViewById(R.id.register_btn);
         //listener

         login_btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 final String email2 = login_email.getText().toString().trim();
                 final String pass = login_email.getText().toString().trim();
                 String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z]+";
                 if (email2.matches(emailPattern))
                 {
                     final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "",
                             "Loading. Please wait...", true);
                     new Login_Handler().
                             login(login_email.getText().toString(), login_password.getText().toString(), new FireCallBack() {
                                 @Override
                                 public void onCallback(Object value) {
                                     Res = (a43pixelz.com.may_admin.modal.LoginResult) value;
                                     if(Res.Result.equals(true)){
                                         sPref.setValue("admin_login","yes");
                                         sPref.setValue("admin_email",login_email.getText().toString());

                                         new Admin_Handler().get(login_email.getText().toString(), new FireCallBack() {
                                             @Override
                                             public void onCallback(Object value) {
                                                 Admin p = (Admin) value;
                                                 sPref.setValue("admin_name",p.getV("name"));
                                             }});
                                         sPref.getValue("admin_email");
                                         sPref.getValue("admin_name");
                                         Intent profile = new Intent(LoginActivity.this, HomeActivity.class);
                                         startActivity(profile);
                                         finish();
                                     }
                                     dialog.dismiss();
                                 }
                             });

                 }
                 else
                 {
                 }

             }
         });
         register_btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 final String user = register_username.getText().toString().trim();
                 final String email2 = register_email.getText().toString().trim();
                 final String pass2=register_password.getText().toString().trim();
                 String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                 if(user.length()>1) {
                     if (email2.matches(emailPattern)) {
                         if (pass2.length() > 5) {
                             final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "",
                                     "Loading. Please wait...", true);
                             UserData = getRegFormData(getApplicationContext());
//                             new Device_Handler().isDeviceExists(UserData, new FireCallBack() {
//                                 @Override
//                                 public void onCallback(Object value) {
//                                     LoginResult Res = (LoginResult) value;
//                                     if (Res.Result == true){
                                        LoginResult Res = new LoginResult();
                                        Res.Msg= "done";
                                        Res.Result = true;
                                        if(true){
                                         new Register_Handler().write(UserData);
                                         finish();
                                         SPref s=new SPref(LoginActivity.this);
                                         s.setValue("admin_login","yes");
                                         s.setValue("admin_email",email2);
                                         s.setValue("admin_name",user);
                                         Intent profile = new Intent(LoginActivity.this, HomeActivity.class);
                                         startActivity(profile);

                                     }
                                     else{

                                        }
                                     dialog.dismiss();
//                                 }
//                             });
                         } else {
                          }
                     } else {
                     }
                 }else{

                 }
             }
         });

    }

    Admin getRegFormData(Context ctx){
        EditText Uname = this.findViewById(R.id.register_username);
        EditText Email = this.findViewById(R.id.register_email);
        EditText Pass = this.findViewById(R.id.register_password);
        String UnameStr, EmailStr, PassStr;
        UnameStr = Uname.getText().toString();
        EmailStr = Email.getText().toString();
        PassStr = Pass.getText().toString();
        return new Admin(UnameStr, EmailStr, PassStr);
    }

}
