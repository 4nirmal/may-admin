package a43pixelz.com.may_admin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import a43pixelz.com.may_admin.handlers.FireCallBack;
import a43pixelz.com.may_admin.handlers.Log_list_handler;
import a43pixelz.com.may_admin.handlers.Users_Handler;
import a43pixelz.com.may_admin.modal.User;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogFragment.
     */
    // TODO: Rename and change types and number of parameters
    ProgressDialog progressDialog;
    public static LogFragment newInstance(String param1, String param2) {
        LogFragment fragment = new LogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    ListView listView;
    UserAdaptor adapter;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final ArrayList<DataModel_Chat_User> DataModel_Chat_Users;
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final View V = view;
        DataModel_Chat_Users= new ArrayList<>();
        new Log_list_handler().getUsers(new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                List<User> Users = (List<User>) value;
                for (User U : Users){
                    DataModel_Chat_Users.add(new DataModel_Chat_User(U.getV("name"),U.getV("email"),"","",U.getV("status")));
                }
// Updated upstream
                adapter= new UserAdaptor(DataModel_Chat_Users,getActivity().getApplicationContext());
                listView=(ListView)V.findViewById(R.id.log_listview);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                progressDialog.dismiss();
//
            }
        });
        adapter= new UserAdaptor(DataModel_Chat_Users,getActivity());
        listView=(ListView)view.findViewById(R.id.log_listview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setAdapter(adapter);
                SPref sPref=new SPref(getActivity());
                DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                sPref.setValue("userid",dataModel.getemail());
                sPref.setValue("username2",dataModel.getname());
                Intent intent=new Intent(getActivity(),LogActivity.class);
                startActivity(intent);
            }
        });
        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
// Stashed changes
                adapter.notifyDataSetChanged();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SPref sPref=new SPref(getActivity().getApplicationContext());
                        DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                        sPref.setValue("userid",dataModel.getemail());
                        sPref.setValue("username2",dataModel.getname());
                        Intent intent=new Intent(getActivity().getApplicationContext(),LogActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });
        super.onViewCreated(view, savedInstanceState);


    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_log, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
