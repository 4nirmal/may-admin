package a43pixelz.com.may_admin;

import java.util.HashMap;
import java.util.Map;

public class Log {
    public Map<String, String> kv ;
    public Log(){kv = new HashMap<>();}
    public Log(String amount, String  date, String  ques,String status,String transaction, String type){
        kv = new HashMap<>();
        kv.put("amount", amount);
        kv.put("datetime", date);
        kv.put("question", ques);
        kv.put("status", status);
        kv.put("type", type);
        kv.put("transaction", transaction);

    }

    public String getV(String K){
        return kv.get(K);
    }
    public Object getKV(){
        return kv;
    }
    public void setV(String K, String V) { kv.put(K, V);}
}
