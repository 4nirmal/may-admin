package a43pixelz.com.may_admin;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import a43pixelz.com.may_admin.modal.ChatMessage;
import a43pixelz.com.may_admin.modal.Status;
import a43pixelz.com.may_admin.modal.User;
import a43pixelz.com.may_admin.modal.UserType;

import static a43pixelz.com.may_admin.handlers.Fire_handler.db;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BroadcastFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BroadcastFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BroadcastFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public BroadcastFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BroadcastFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BroadcastFragment newInstance(String param1, String param2) {
        BroadcastFragment fragment = new BroadcastFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    CheckBox rasicheckbox;
    CheckBox lagnamcheckbox;
    CheckBox dasacheckbox;
    CheckBox yearcheckbox;
    CheckBox monthcheckbox;
    CheckBox datecheckbox;
    Spinner rasiSpinner;
    Spinner lagnamSpinner;
    Spinner dasaSpinner;
    EditText yearEdittext;
    EditText monthEdittext;
    EditText dateEdittext;
    Button search;
    ListView listView;

    EditText B_Edittext;
    ImageButton b_send;
    String admin_name;
    SPref sPref;
    private ArrayList<ChatMessage> chatMessages;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        rasicheckbox=view.findViewById(R.id.rasicheckbox);
        lagnamcheckbox=view.findViewById(R.id.lagnamcheckbox);
        dasacheckbox=view.findViewById(R.id.dasacheckbox);
        yearcheckbox=view.findViewById(R.id.yearcheckbox);
        monthcheckbox=view.findViewById(R.id.monthcheckbox);
        datecheckbox=view.findViewById(R.id.datecheckbox);
listView=view.findViewById(R.id.broadcast_listview);

        B_Edittext=view.findViewById(R.id.broadcast_edittext);
        b_send=view.findViewById(R.id.broadcast_send);

        //Spinner

        String[] rasi1=new String[]{"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருச்சிகம்","தனுசு","மகரம்","கும்பம்","மீனம்"};
        ArrayAdapter<String> rasi=new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_dropdown_item,rasi1);
        rasi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rasiSpinner=(Spinner)view.findViewById(R.id.rasispinner2);
        rasiSpinner.setAdapter(rasi);

        String[] lagnam1=new String[]{"மேஷம்","ரிஷபம்","மிதுனம்","கடகம்","சிம்மம்","கன்னி","துலாம்","விருச்சிகம்","தனுசு","மகரம்","கும்பம்","மீனம்"};
        ArrayAdapter<String> lagnam=new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_dropdown_item,lagnam1);
        lagnam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lagnamSpinner=(Spinner)view.findViewById(R.id.lagnamspinner2);
        lagnamSpinner.setAdapter(lagnam);

        String[] Dasa1=new String[]{"சூரியன்","சந்திரன்","செவ்வாய்","புதன்","குரு","சுக்கிரன்","சனி","ராகு","கேது"};
        ArrayAdapter<String> Dasa=new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_dropdown_item,Dasa1);
        Dasa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dasaSpinner=(Spinner)view.findViewById(R.id.dasaspinner2);
        dasaSpinner.setAdapter(Dasa);


        yearEdittext=view.findViewById(R.id.yearedittext);
        monthEdittext=view.findViewById(R.id.monthedittext);
        dateEdittext=view.findViewById(R.id.dateedittext);

        //button
        search=view.findViewById(R.id.searchbutton);
        progressDialog=new ProgressDialog(getActivity());

        adapter= new UserAdaptor(DataModel_Chat_Users,getActivity().getApplicationContext());
        listView.setAdapter(adapter);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rasi();
                progressDialog.show();
            }
        });
        b_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                for (int i=0;i<newlist.size();i++){
                    loadFireData(newlist.get(i));

               }
            }
        });
        chatMessages = new ArrayList<>();
        sPref=new SPref(getActivity());
        admin_name=sPref.getValue("admin_name");
        listAdapter = new ChatListAdapter(chatMessages, getActivity());
    }

    DatabaseReference mFirebaseDatabase;
    private  void loadFireData(final String userid)
    {
        mFirebaseDatabase = db.getReference("chats").child(userid);
        mFirebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chatMessages.clear();
                for (DataSnapshot msgSnapshot: dataSnapshot.getChildren()) {
                    ChatMessage msg = msgSnapshot.getValue(ChatMessage.class);
                    chatMessages.add(msg);
                }
                sendMessage(B_Edittext.getText().toString(),UserType.OTHER,userid);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void sendMessage(final String messageText, final UserType userType,String user)
    {
        if(messageText.trim().length()==0)
            return;
        //ToDo: Change this
        fireBaseHandler(messageText, userType, "",user);
    }
    private ChatListAdapter listAdapter;

    private void fireBaseHandler(final String messageText, final UserType userType, String ses,String user)
    {
        ChatMessage message = new ChatMessage();
        message.setMessageStatus(Status.REPLIED);
        message.setMessageText(messageText);
        message.setUserType(userType);
        message.setMessageTime(new Date().getTime());
        //ToDo: change this
        message.setUser(admin_name);
        chatMessages.add(message);
        //ToDo: change this
        db.getReference("chats").child(user).setValue(listAdapter.getMessages());

    }
    ProgressDialog progressDialog;
    public void rasi(){
        final ArrayList<String> finallist=new ArrayList<>();
        if(rasicheckbox.isChecked()) {

            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if (dsp.child("rasi").getValue()==null){

                        }
                        else if(dsp.child("rasi").getValue().toString().matches(rasiSpinner.getSelectedItem().toString())) {
                            finallist.add(dsp.getKey());
                        }
                    }
                    lagnam(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else {
            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        finallist.add(dsp.getKey());

                    }
                    lagnam(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }
    public void lagnam(final ArrayList<String> list){
        final ArrayList<String> finallist=new ArrayList<>();
        if(lagnamcheckbox.isChecked()) {

            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            if (dsp.child("lagnam").getValue()==null){

                            }
                            else if(dsp.child("lagnam").getValue().toString().matches(lagnamSpinner.getSelectedItem().toString())) {
                                finallist.add(dsp.getKey());
                            }
                        }
                    }
                    dasa(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else {
            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            finallist.add(dsp.getKey());

                        }
                    }
                    dasa(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void dasa(final ArrayList<String> list){
        final ArrayList<String> finallist=new ArrayList<>();
        if(dasacheckbox.isChecked()) {

            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            if (dsp.child("dasa").getValue()==null){

                            }
                            else if(dsp.child("dasa").getValue().toString().matches(dasaSpinner.getSelectedItem().toString())) {
                                finallist.add(dsp.getKey());
                            }
                        }
                    }
                    year(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else {
            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            finallist.add(dsp.getKey());

                        }
                    }
                    year(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void year(final ArrayList<String> list){
        final ArrayList<String> finallist=new ArrayList<>();
        if(yearcheckbox.isChecked()) {

            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            if (dsp.child("year").getValue()==null){

                            }
                            else if (dsp.child("year").getValue().toString().substring(0,dsp.child("year").getValue().toString().indexOf("/")).matches(yearEdittext.getText().toString())) {
                                finallist.add(dsp.getKey());
                            }
                        }
                    }
                    month(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else {
            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            finallist.add(dsp.getKey());

                        }
                    }
                    month(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void month(final ArrayList<String> list){
        final ArrayList<String> finallist=new ArrayList<>();
        if(monthcheckbox.isChecked()) {

            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            if (dsp.child("year").getValue()==null){

                            }
                            else if (dsp.child("year").getValue().toString().substring(dsp.child("year").getValue().toString().indexOf("/")+1,dsp.child("year").getValue().toString().indexOf("-")).matches(monthEdittext.getText().toString())) {
                                finallist.add(dsp.getKey());
                            }
                        }
                    }
                    ddate(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else {
            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            finallist.add(dsp.getKey());

                        }
                    }
                    ddate(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    UserAdaptor adapter;
    final List<User> Res = new ArrayList<User>();
    ArrayList<DataModel_Chat_User> DataModel_Chat_Users = new ArrayList<>();
    public void ddate(final ArrayList<String> list){
        final ArrayList<String> finallist=new ArrayList<>();
        if(datecheckbox.isChecked()) {

            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            if (dsp.child("year").getValue()==null){

                            }
                            else if (dsp.child("year").getValue().toString().substring(dsp.child("year").getValue().toString().indexOf("-")+1).matches(monthEdittext.getText().toString())) {
                                finallist.add(dsp.getKey());
                            }
                        }
                    }
                    updateui(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }else {
            DatabaseReference myRef = db.getReference("profile/");
            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot     dataSnapshot) {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        if(list.contains(dsp.getKey())){
                            finallist.add(dsp.getKey());

                        }
                    }
                    updateui(finallist);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    ArrayList<String> newlist;
   public void updateui(final ArrayList<String> list){
       DataModel_Chat_Users.clear();
       adapter.clear();
       Res.clear();
       newlist=new ArrayList<>();
        DatabaseReference myRef = db.getReference("profile/");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                    if(list.contains(dsp.getKey())){
                        newlist.add(dsp.getKey());
                        Res.add(new User(dsp.getKey(),dsp.getKey(),"","",""));
                    }
                }
                final  List<User> Users = Res;
                for (User U : Users){
                    DataModel_Chat_Users.add(new DataModel_Chat_User(U.getV("name"),U.getV("email"),U.getV("password"),U.getV("mac"),U.getV("status")));

                }

               getActivity().runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       adapter.notifyDataSetChanged();
                       progressDialog.dismiss();
                   }
               });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_broadcast, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
