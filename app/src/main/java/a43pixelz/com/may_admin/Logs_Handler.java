package a43pixelz.com.may_admin;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

import a43pixelz.com.may_admin.handlers.FireCallBack;

import static a43pixelz.com.may_admin.handlers.Fire_handler.db;

public class Logs_Handler {
    ArrayList<Log> Res;
    Log log = new Log();

    public boolean write(String user,String datetime,String amount,String question,String status,String transaction,String type) {

        String UID = user;
        Log log = new Log(amount, datetime, question, status,transaction, type);
        db.getReference("logs").child(UID).push().setValue(log.getKV());
        return true;
    }

    public ArrayList<Log> get (final String Email, final FireCallBack FCB){
        Res = new ArrayList<Log>();
        String UID = Email;
        DatabaseReference myRef = db.getReference("logs/" + UID);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    Map<String, String> KV = (Map<String, String>) ds.getValue();
                    log = new Log();
                    log.kv = KV;
                    Res.add(log);
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }


    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }
}