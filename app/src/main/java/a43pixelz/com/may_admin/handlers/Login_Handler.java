package a43pixelz.com.may_admin.handlers;

/**
 * Created by nirmal on 12/5/18.
 */

import a43pixelz.com.may_admin.modal.*;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import a43pixelz.com.may_admin.modal.LoginResult;


public class Login_Handler implements Fire_handler{
    LoginResult Res;

    public LoginResult login (final String Email, final String PassWord, final FireCallBack FCB){
        Res = new LoginResult();

        String UID = getUserId(Email);
        DatabaseReference myRef = db.getReference("admin/" + UID);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("email")){
                    Map<String, String> KV = (Map<String, String>) dataSnapshot.getValue();
                    if(KV.get("email").equals(Email) && KV.get("password").equals(PassWord)){
                        Res.Result = true;
                        Res.Msg = "Success";
                    }
                    else {
                        Res.Result = false;
                        Res.Msg = "Invalid Creds..";
                    }
                }
                else {
                    Res.Result = false;
                    Res.Msg = "User Does Not Exists";
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }
    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }

}
