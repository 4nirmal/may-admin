package a43pixelz.com.may_admin.handlers;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import a43pixelz.com.may_admin.modal.LoginResult;
import a43pixelz.com.may_admin.modal.User;

/**
 * Created by nirmal on 13/5/18.
 */

public class Device_Handler implements  Fire_handler{
    LoginResult Res;
    String UID;

    public boolean isDeviceExists(final User Usr, final FireCallBack FCB) {
        String Email = Usr.getV("email");
        UID = getUserId(Email);
        Res = new LoginResult();
        Res.Result = true;

        DatabaseReference myRef = db.getReference("devices");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                   Map<String, String> F = (Map<String, String>) dataSnapshot.getValue();
                    if(F.containsKey(UID)){
                        // has same email id
                        Res.Result = false;
                        Res.Msg = "Illegal Attempt";
                        break;
                    }
                    if(F.containsValue(Usr.getV("mac"))){
                        Res.Result = false;
                        Res.Msg = "Illegal Attempt";
                        break;
                    }
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return true;
    }

    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }
}
