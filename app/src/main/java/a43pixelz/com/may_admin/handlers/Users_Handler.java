package a43pixelz.com.may_admin.handlers;
/**
 * Created by nirmal on 12/5/18.
 */

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import a43pixelz.com.may_admin.modal.User;


public class Users_Handler implements Fire_handler {

    public List<User> getUsers (final FireCallBack FCB){
        final List<User> Res = new ArrayList<User>();

        DatabaseReference myRef = db.getReference("profile/");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                    try{
                        Res.add(new User(String.valueOf(dsp.child("name").getValue()),dsp.getKey(),"","",""));

                    /*    Map<String, String> Val = (Map<String, String>) dsp.getValue();
                        Res.add(new User(Val.get("name"), Val.get("email"), "", ""));
                    */}catch (Exception e){}
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }
}
