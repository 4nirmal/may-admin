package a43pixelz.com.may_admin.handlers;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import a43pixelz.com.may_admin.modal.Profile;

public class Profile_handler implements Fire_handler {
    Profile Res;

    public boolean write(final Profile profile) {
        String Email = profile.getV("email");
        String UID = getUserId(Email);

        DatabaseReference myRef = db.getReference("profile/" + UID);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().setValue(profile.getKV());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return true;
    }

    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }

    public Profile get (final String Email, final FireCallBack FCB){
        Res = new Profile();
        String UID = getUserId(Email);
        DatabaseReference myRef = db.getReference("profile/" + UID);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("email")){
                    Map<String, String> KV = (Map<String, String>) dataSnapshot.getValue();
                    Res.kv = KV;
                }
                else {
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }

}
