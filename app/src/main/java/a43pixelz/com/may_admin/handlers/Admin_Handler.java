package a43pixelz.com.may_admin.handlers;

/**
 * Created by Nirmal on 7/23/2018.
 */

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import a43pixelz.com.may_admin.modal.Admin;

/**
 * Created by nirmal on 12/5/18.
 */

public class Admin_Handler implements Fire_handler{
    Admin Res;

    public Admin get(final String Email, final FireCallBack FCB){
        Res = new Admin();

        String UID = getUserId(Email);
        DatabaseReference myRef = db.getReference("admin/" + UID);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("email")){
                    Map<String, String> KV = (Map<String, String>) dataSnapshot.getValue();
                    Res = new Admin(KV.get("name"), KV.get("email"), KV.get("password"));
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }
    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }

}
