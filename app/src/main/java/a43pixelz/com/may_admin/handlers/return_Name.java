package a43pixelz.com.may_admin.handlers;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import a43pixelz.com.may_admin.modal.Profile;

public class return_Name implements Fire_handler {
    String Res;



    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }

    public String return_Name(final String Email, final FireCallBack FCB){
        String UID = getUserId(Email);
        DatabaseReference myRef = db.getReference("profile/" + UID);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("email")){
                    Map<String, String> KV = (Map<String, String>) dataSnapshot.getValue();
                    Res= KV.get("name");
                }
                else {
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }

}