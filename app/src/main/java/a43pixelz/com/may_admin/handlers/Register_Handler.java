package a43pixelz.com.may_admin.handlers;

import a43pixelz.com.may_admin.modal.Admin;
import a43pixelz.com.may_admin.modal.LoginResult;
import a43pixelz.com.may_admin.modal.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by nirmal on 8/5/18.
 */

public class Register_Handler implements Fire_handler{
    LoginResult Res;

    public boolean write(final Admin Usr) {
        String Email = Usr.getV("email");
        String UID = getUserId(Email);

        DatabaseReference myRef = db.getReference("admin/" + UID);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().setValue(Usr.getKV());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return true;
    }
    private String getUserId(String Email){
        try {
            return Email.substring(0, Email.indexOf("@"));
        }catch (Exception e){
            return Email;
        }
    }
}
