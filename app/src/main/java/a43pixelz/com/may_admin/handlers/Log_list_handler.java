package a43pixelz.com.may_admin.handlers;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import a43pixelz.com.may_admin.modal.User;

public class Log_list_handler implements Fire_handler {

    public List<User> getUsers (final FireCallBack FCB){
        final List<User> Res = new ArrayList<User>();

        DatabaseReference myRef = db.getReference("logs/");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                    String update="";
                    for (DataSnapshot dsp2 : dsp.getChildren()) {
                        update=String.valueOf(dsp2.child("status").getValue());
                    }try{
                        Res.add(new User(dsp.getKey(),dsp.getKey(),"","",update));
                   /*     Map<String, String> Val = (Map<String, String>) dsp.getValue();
                        Res.add(new User(Val.get("name"), Val.get("email"), "", ""));
                   */ }catch (Exception e){}
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }

}
