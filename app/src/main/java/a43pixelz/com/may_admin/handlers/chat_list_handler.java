package a43pixelz.com.may_admin.handlers;
/**
 * Created by nirmal on 12/5/18.
 */

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import a43pixelz.com.may_admin.modal.ChatMessage;
import a43pixelz.com.may_admin.modal.Status;
import a43pixelz.com.may_admin.modal.User;


public class chat_list_handler implements Fire_handler {

    public List<User> getUsers (final FireCallBack FCB){
        final List<User> Res = new ArrayList<User>();

        DatabaseReference myRef = db.getReference("chats/");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsp : dataSnapshot.getChildren()) {

                    String status="";
                    String user="";
                    String message="";
                    String time="";
                        for (DataSnapshot msgSnapshot: dsp.getChildren()) {
                            time=String.valueOf(msgSnapshot.child("messageTime").getValue());
                            user=String.valueOf(msgSnapshot.child("user").getValue());
                            status=String.valueOf(msgSnapshot.child("messageStatus").getValue());
                            message=String.valueOf(msgSnapshot.child("messageText").getValue());
                        }
                    try{
                        message=  message.substring(0,20)+"...";
                        }
                    catch(Exception e){

                    }
                        Res.add(new User(user, dsp.getKey(),  message,time,status));
                }
                FCB.onCallback(Res);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Res;
    }

}
