package a43pixelz.com.may_admin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class autostart extends BroadcastReceiver {

    @Override
    public void onReceive(Context aContext, Intent aIntent) {
        // This is where you start your service
        try {
            Intent intent1=new Intent(aContext, Push_Message.class);
            intent1.setPackage(aContext.getPackageName());
            aContext.startService(intent1);

        }
        catch (Exception e) {
            aContext.startActivity(new Intent(aContext,HomeActivity.class));
        }

    }
}
