package a43pixelz.com.may_admin;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class customlistview extends ArrayAdapter<listitemmodal> implements View.OnClickListener{

    private ArrayList<listitemmodal> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView question;
        TextView amount;
        TextView status;
        TextView datetime;
    }

    public customlistview(ArrayList<listitemmodal> data, Context context) {
        super(context, R.layout.logitem, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        listitemmodal dataModel=(listitemmodal)object;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        listitemmodal dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        customlistview.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new customlistview.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.logitem, parent, false);
            viewHolder.question = (TextView) convertView.findViewById(R.id.log_question);
            viewHolder.amount = (TextView) convertView.findViewById(R.id.log_amount);
            viewHolder.status = (TextView) convertView.findViewById(R.id.log_status);
            viewHolder.datetime = (TextView) convertView.findViewById(R.id.log_datetime);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (customlistview.ViewHolder) convertView.getTag();
            result=convertView;
        }



        viewHolder.question.setText(String.valueOf(dataModel.getquestion()+" questions"));
        viewHolder.amount.setText(String.valueOf("\u20B9"+dataModel.getamount()));
        viewHolder.datetime.setText(String.valueOf(dataModel.getdatetime()));
        viewHolder.status.setText(String.valueOf(dataModel.getstatus()));
        if(dataModel.getstatus().matches("Success")){
            viewHolder.status.setTextColor(Color.rgb(44, 142, 22));
        }else{
            viewHolder.status.setTextColor(Color.RED);
        }

        // Return the completed view to render on screen
        return convertView;
    }
}
