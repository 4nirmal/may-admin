package a43pixelz.com.may_admin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import a43pixelz.com.may_admin.R;
import a43pixelz.com.may_admin.handlers.FireCallBack;
import a43pixelz.com.may_admin.handlers.Users_Handler;
import a43pixelz.com.may_admin.modal.User;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public UserListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserListFragment newInstance(String param1, String param2) {
        UserListFragment fragment = new UserListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    ListView listView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * has returned, but before any saved state has been restored in to the view.
     * This gives subclasses a chance to initialize themselves once
     * they know their view hierarchy has been completely created.  The fragment's
     * view hierarchy is not however attached to its parent at this point.
     *
     * @param view               The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     */
    UserAdaptor adapter;
    ProgressDialog progressDialog;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final ArrayList<DataModel_Chat_User> DataModel_Chat_Users;
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.show();
        final View V = view;
        DataModel_Chat_Users= new ArrayList<>();

        new Users_Handler().getUsers(new FireCallBack() {
            @Override
            public void onCallback(Object value) {
                List<User> Users = (List<User>) value;
                for (User U : Users){
                    DataModel_Chat_Users.add(new DataModel_Chat_User(U.getV("name"),U.getV("email"),"","",""));

                }

// Updated upstream
                adapter= new UserAdaptor(DataModel_Chat_Users,getActivity().getApplicationContext());
                listView=(ListView)V.findViewById(R.id.user_list);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();



                progressDialog.dismiss();
//
            }
        });

        adapter= new UserAdaptor(DataModel_Chat_Users,getActivity());
        listView=(ListView)view.findViewById(R.id.user_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                SPref sPref=new SPref(getActivity());
                DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                sPref.setValue("userid",dataModel.getemail());
                sPref.setValue("username2",dataModel.getname());
                Intent intent=new Intent(getActivity(),UserActivity.class);
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setAdapter(adapter);
                SPref sPref=new SPref(getActivity());
                DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                sPref.setValue("userid",dataModel.getemail());
                sPref.setValue("username2",dataModel.getname());
                Intent intent=new Intent(getActivity(),ChatActivity.class);
                startActivity(intent);
           return true;
            }
        });
        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
// Stashed changes
                adapter.notifyDataSetChanged();

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SPref sPref=new SPref(getActivity().getApplicationContext());
                        DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                        sPref.setValue("userid",dataModel.getemail());
                        sPref.setValue("username2",dataModel.getname());
                        Intent intent=new Intent(getActivity().getApplicationContext(),UserActivity.class);
                        startActivity(intent);
                    }
                });
                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        listView.setAdapter(adapter);
                        SPref sPref=new SPref(getActivity());
                        DataModel_Chat_User dataModel= DataModel_Chat_Users.get(position);
                        sPref.setValue("userid",dataModel.getemail());
                        sPref.setValue("username2",dataModel.getname());
                        Intent intent=new Intent(getActivity(),ChatActivity.class);
                        startActivity(intent);
                        return true;
                    }
                });
            }
        });
        super.onViewCreated(view, savedInstanceState);
        //List view ADDing;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_user_list, container, false);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
